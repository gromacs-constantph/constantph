Constant pH Molecular Dynamics
------------------------------

Multisite titratable groups
---
If titratable sites are "chemically" or "topologically" coupled, the force field parameters of one site depend on the value of the $`\lambda`$ coordinate of the other site, and *vice versa*. For example, in most force fields, the partial charges of all atoms in the histidine side chain, including the two titratable sites, depend on the protonation state.

To model chemically coupled sites, the multisite representation of titratable groups is used [1], where a separate $`\lambda_{i,k}`$-coordinate is assigned to each physical state $`k`$ of a titratable group $`i`$. For a residue with multiple "chemically"-coupled titratable sites each $`\lambda_{i,k}`$ -coordinate has the same state at $`\lambda_{i,k}=0`$, while at $`\lambda_{i,k}=1`$ the group is in one of the $`n_i`$ possible protonation states (i.e., state $`k`$) of residue $`i`$. The state at $`\lambda_{i,k}=0`$ (for all coordinates $`k`$ of residue $`i`$) is the same for all $`\lambda`$-coordinates but does not correspond to a physical protonation state of the residue, and neither do states for which $`\sum_k \lambda_{i,k} \neq 1`$. To restrict sampling to the (hyper-)plane connecting the physical states,  the sum of $`\lambda_{i,k}`$-s is constrained ($`\sum_k\lambda_{i,k} = 1`$). The constraint is fullfiled with a generalized version of charge constraint introduced by Donnini *et al.* [2][3].  

In the multisite representation, each  $`\lambda`$-coordinate is independent of the others and thus evolves on a one dimensional potential, similar to that of "chemically" uncoupled sites. However, in contrast to the uncoupled sites, the correction potential $`V^\text{MM}`$ is multi-dimensional as its value depends on all $`\lambda_{i,k}`$ coordinates representing each of the possible protonation states of residue $`i`$. These potentials are obtained through a least-square fit of a multi-dimensional polynomial to the ensemble-averaged gradients of the potentials with respect to $`\lambda_{i,k}`$ evaluated on the  ($`{n_i}-1`$)-dimensional grid of the $`n_i`$ coupled $`\lambda`$-s, *i.e.*: $`\langle\partial V/\partial\lambda_{i,k}\rangle_{\lambda_1...\lambda_{n_i}}`$. The fitting procedure is not the part of the constant pH code, but is essential for accurate simulations. The fitting code can be retrieved from the supplementary files of [2].

Charge constraint
---

Dynamically changing partial charges can affect the total charge of the simulation unit cell, which can lead to artifacts, as documented for instance in Hub *et al.* for Ewald-based methods [4]. To avoid such artifacts, it is essential to keep the total charge of the unit cell constant. This is achieved by adding titratable buffer particles to the system that collectively compensate changes in charge of all titratable residues [2][3]. Optimally, the buffer particles should keep their charges in range between $`-0.5~e`$ and $`0.5~e`$ and their number should be sufficient to compensate for the charge changes of the simulated system.

References
---
[1]: https://doi.org/10.1021/ct200444f Multisite $`\lambda`$-Dynamics for Simulated Structure–Activity Relationship Studies Knight J.L., Brooks III C.L., JCTC, 2011

[2]: https://doi.org/10.26434/chemrxiv-2022-n025t-v2  Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., ChemRxiv, 2022

[3]: https://doi.org/10.1021/acs.jctc.5b01160 Charge-Neutral Constant pH Molecular Dynamics Simulations Using a Parsimonious Proton Buffer, Donnini S., Ullmann R.T., Groenhof G., Grubmüller H., JCTC, 2016

[4]: https://doi.org/10.1021/ct400626b Quantifying Artifacts in Ewald Simulations of Inhomogeneous Systems with a Net Charge,     Hub J.S., de Groot B.L., Grubmüller H., Groenhof G., JCTC, 2014
