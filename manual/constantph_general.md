Constant pH Molecular Dynamics: Basics of the method
------------------------------

The full description of the methodology can be found in Aho *et al.* [1]. In constant pH molecular dynamics based on $`\lambda`$-dynamics [2], an additional coordinate $`\lambda_i`$ is introduced for each titratable site i in the system, and interactions of the total system are continuously interpolated between the protonated and deprotonated states along this coordinate. During the simulation, the $`\lambda`$-coordinates are updated along with cartesian coordinates. The total Hamiltonian of the system with additional $`\lambda`$-coordinates is

```math
H(\mathbf{R},\mathbf{\lambda}) = \sum_i^{N_{\text{sites}}}\frac{m_\lambda}{2}\dot{\lambda}_i^2 
          +\sum_j^{N_{\text{atoms}}}\frac{m_j}{2}\dot{\mathbf{r}}_j^2+V({\mathbf{R}},{\boldsymbol{\lambda}})
```

where $`\mathbf{R}`$ is the vector of the cartesian coordinates $`\mathbf{r}_j`$ of all $`N_{\text{atoms}}`$ atoms with mass $`m_j`$, and $`\boldsymbol{\lambda}`$ the vector of the $`\lambda`$-coordinates of all sites $`N_{\text{sites}}`$ with mass $`m_{\lambda}`$.

The $`\lambda`$-dependent potential term $`V({\mathbf{R}},{\boldsymbol{\lambda}})`$ can be written as

```math
V({\mathbf{R}},\boldsymbol{\lambda}) = V_{\text{coul}}({\mathbf{R}},\boldsymbol{\lambda}) +
          V^{\text{MM}}(\boldsymbol{\lambda})+V^{\text{bias}}(\boldsymbol{\lambda})+V^\text{pH}(\boldsymbol{\lambda})
```

where $`V_{\text{coul}}({\mathbf{R}},\boldsymbol{\lambda})`$ is the electrostatic Coulombic energy interpolated between the protonated and deprotonated states. The three additional $`\lambda`$-dependent terms are (i) a correction potential $`V_i^{\text{MM}}(\lambda_i)`$ to compensate for missing quantum mechanical contributions to proton affinities of titratable group i; (ii) a biasing potential $`V_i^\text{bias}(\lambda_i)`$ that enhances sampling of the physical end states at $`\lambda_i=0`$ and $`\lambda_i=1`$ for titratable group i; and (iii) a pH-dependent term $`V^\text{pH}(\lambda_i)`$ to model the chemical potential of protons in water for titratable group i.

1. correction term  $`V_i^{\text{MM}}(\lambda_i)`$ makes the interpolated potential function flat if the titratable site i is in its reference state, for which the proton affinity is known experimentally, at $`\text{pH}=\text{p}K_{\text{a},i}`$. This potential is determined by evaluating the deprotonation free energy of the single residue in water (reference state) at the force field level:

```math
    V_i^{\text{MM}}(\lambda_i) = -\Delta G_i^{\text{MM}}(\lambda_i)
```

2. biasing potential $`V_i^\text{bias}(\lambda_i)`$ is added to prevent sampling of the non-physical states between $`\lambda_i=0`$ and $`\lambda_i=1`$ on this flat potential energy surface while still enabling sufficient transitions between the physical end-states to sample both protonation states with the correct thermodynamic weight. We use the form of the biasing potential as suggested by Donnini et.al [3]:

```math
    \begin{aligned}
          V_i^\text{bias}(\lambda) &= -k\left[\exp\left(-\frac{(\lambda-1-b)^2}{2a^2} \right) 
          + \exp \left(-\frac{(\lambda+b)^2}{2a^2} \right) \right] 
          + d \left[ \exp\left(-\frac{(\lambda-0.5)^2}{2s^2} \right) \right] \\
          &+ 0.5w\bigg( \Big(1-\text{erf}[r(\lambda+m)]\Big) + \Big(1 + \text{erf}[r(\lambda-1-m)] \Big) \bigg)
          \end{aligned}
``` 
where the code computes the parameters $`k`$, $`a`$, $`b`$, $`d`$, $`s`$, $`w`$, $`r`$ and $`m`$ as a function of barrier height, which is provided by the user [1,3].

3. the potential describing the pH-dependence includes the effect of the solution pH on the free energy difference between the protonated and deprotonated states, and is described as:

```math
\begin{aligned}
          V^\text{pH}(\lambda_i) &= RT\ln(10)\left[\text{pK}_{\text{a},i}-\text{pH}\right] \frac{1}{1+\exp(-2 k_1 (\lambda_i-1+x_0)} \text{, if pH }> \text{p}K_{\text{a}} \\
          V^\text{pH}(\lambda_i)&=RT\ln(10)\left[\text{pK}_{\text{a},i}-\text{pH}\right] \frac{1}{1+\exp(-2 k_1 (\lambda_i-x_0)} \text{, if pH } \leq \text{p}K_{\text{a}}
          \end{aligned}
```
where $`k_1`$ and $`x_0`$ depend on the parameters $`r`$ and $`a`$ of biasing potential ($`k_1 = 2.5 r`$ and $`x_0 = 2a`$).

References
---
[1]: https://doi.org/10.26434/chemrxiv-2022-n025t-v2  Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., ChemRxiv, 2022

[2]: https://doi.org/10.1063/1.472109 $`\lambda`$‐dynamics: A new approach to free energy calculations, Kong X and Brooks III C.L., JCP, 1996

[3]: https://doi.org/10.1021/acs.jctc.5b01160 Charge-Neutral Constant pH Molecular Dynamics Simulations Using a Parsimonious Proton Buffer, Donnini S., Ullmann R.T., Groenhof G., Grubmüller H., JCTC, 2016



