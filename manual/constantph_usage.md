Usage
---

Currently, all input parameters for CpHMD are defined in the $`{\tt .mdp}`$ file. In practise, to run a CpHMD simulation, all types of titratable groups must be described. Also the mapping from atom sets to titratable groups must be provided. The description of a titratable group includes (i) the partial charges for all protonation states, (ii) the reference $`\text{p}K_{\text{a}}`$ value of all titratable sites, and (iii) the coefficients of the $`V^{\text{MM}}`$ polynomial fits. The mapping includes the name of index group from $`{\tt index.ndx}`$ file, the name of corresponding titratable group type, and the initial $`\lambda`$-values.

The simulation system can be constructed similar to standard GROMACS. All titratable sites must include the hydrogen atom explicitly. Upon deprotonation, the charge of this hydrogen atom will be set to zero, but the atom will physically be there.

During the simulation, the $`\lambda`$-coordinates are updated along the positions. The output of CpHMD is written in $`{\tt *.edr}`$ file. The $`\lambda`$-coordinates, velocities and $`\partial V/\partial \lambda`$ can be extracted from $`{\tt *.edr}`$ file using `gmx cphmd` command. The details on the usage can be found after description of $`{\tt .mdp}`$ options.

The energy contribution from $`\lambda`$-coordinates is included in the energy ($`{\tt .edr}`$) file. 

$`{\tt .mdp}`$ options
---

These options enable and control the application of constant pH MD. 

- **lambda-dynamics**

   - **no**: Constant pH MD off. 

   - **yes**: Run molecular dynamics at constant pH. User needs to specify the desired pH in `lambda-dynamics-simulation-ph`, the number of group types in `lambda-dynamics-number-lambda-group-types` and number of atom sets in `lambda-dynamics-number-atom-collections`. At least one ``atom-set`` and the corresponding ``group type`` have to be described. Output of constant pH MD (lambda coordinates) will be written in $`{\tt *.edr}`$  and can be extracted using `gmx cphmd` command.
      
- **lambda-dynamics-simulation-ph**
   
   - (0) pH, at which the constant pH simulation will be run.
   
- **lambda-dynamics-update-nst**

   - (100) [steps] Update frequency for $`\lambda`$-coordinate output.
   
- **lambda-dynamics-lambda-particle-mass**

   - (5.0) Mass of the $`\lambda`$-particles.
   
- **lambda-dynamics-tau**

   - (2.0) [ps] time constant for coupling the $`\lambda`$-particles with v-rescale thermostat for lambdas
   
- **lambda-dynamics-number-atom-collections**

   - Total number of atom collections (ie. titratable groups) in the system. There must be the same number of ``atom-set`` entries defined in the ``.mdp`` file. Each ``atom-set`` needs to have a name corresponding to one of the ``group-type`` entries. The parameters of that ``group-type`` will be used for the $`\lambda`$-coordinates of this ``atom-set``. Below only options for atom set 1 are given, further atom collections simply increase the atom set index number.
   
- **lambda-dynamics-number-lambda-group-types**

   - Number of different ``group-type`` entries. There must be the same number of ``group-type`` entries defined in the ``.mdp`` file. Below only options for one group type are given, further ``group-type`` entries simply increase the group type number. 
   
- **lambda-dynamics-multistate-constraints**

   - **no**: This is the default. All titratable groups consist of two states only, described using one $`\lambda`$-coordinate. Each ``group-type`` must have the charges of protonated state in ``-state-0-charges`` and those of deprotonated state in ``-state-1-charges``. Only ``-state-1-reference-pka`` and ``-state-1-dvdl-coefficients`` need to be specified. Also, the corresponding ``atom-set-initial-lambda`` entries have one value specified. 
     
   - **yes**: Each multisite ``group-type`` must have a set of common state charges in ``-state-0-charges``. Charges of each protonation state are given in ``-state-1-charges``, ``-state-2-charges``etc. For each state, also reference $`\text{p}K_{\text{a}}`$ (``-state-1-reference-pka``, etc.) and $`V^{\text{MM}}`$ fit polynomial coefficients (``-state-1-dvdl-coefficients``, etc.) are needed (see ``lambda-dynamics-group-type1``).
     
- **lambda-dynamics-charge-constraints**

   - **no**: This is the default. Don't use charge constraints. Then, either the simulation box won't be neutral (as adviced), or one is directly coupling each titratable group to a co-ion and hence conserving the neutrality of the simulation box.
     
   - **yes**: Charge constraint scheme is used to maintain the box neutrality during constant pH. In this case, one ``group-type`` must be the buffer group with corresponding parameters, and one ``atom-set`` must consist of all the buffer atoms. All the constrained groups need to have same mdp: `lambda-dynamics-atom-set1-charge-restraint-group-index` value. 

- **lambda-dynamics-calibration**

   - **no**: Normal constant pH.
 
   - **yes**: Constant pH run with fixed $`\lambda`$-values (no $`\lambda`$ update during the run). $`\lambda`$-values of ``atom-set1-initial-lambda`` are used. This is useful for obtaining $`V^{\text{MM}}`$ correction potentials. During such runs $`V^{\text{bias}}`$ and $`V^{\text{pH}}`$ potential terms are ignored and only the direvatives of the enviromental potentials with respect to $`\lambda`$ for a fixed $`\lambda`$ are recorded.
     
The following entries consider a ``group-type``. Each titratable group, namely ``atom-set``, is
mapped to one of the group types and its parameters.

- **lambda-dynamics-group-type1-name**

   - Name for a group type. Usually a short name with few uppercase letters (as a residue name 
   in topology). This name is used to map ``atom-set`` to the ``group-type``.

- **lambda-dynamics-group-type1-n-states**

   - (1) Number of protonation states of the titratable group. If 1, then only one $`\lambda`$ coordinate is used and ``state-0-charges``, ``state-1-charges`` define the protonated and deprotonated charges, respectively, and no multisite constraints are used. If > 1, then the multistate constraints are on and ``state-0-charges`` defines the charges of common state, and ``state-*-charges`` defines the charges of the corresponding titratable site protonation state.

- **lambda-dynamics-group-type1-state-0-charges**

   - Partial charges for titratable atoms at $`\lambda=0`$. If `lambda-dynamics-group-type1-n-states = 1`, these refer to the partial charges of the protonated titratable group. For multisite groups ``state-0-charges`` define a common state for the of constrained $`\lambda`$-coordinates constituting the titratable group. Often for multisite groups, the ``state-0-charges`` are set to zero. **Note: The number of charge values must be the same as the number of atoms in the corresponding** ``atom-set``. 

- **lambda-dynamics-group-type1-state-1-charges**
   
   - Partial charges for titratable atoms at $`\lambda=1`$. If `lambda-dynamics-group-type1-n-states = 1`, these refer to the partial charges of the deprotonated titratable group. For multisite groups ``state-1-charges``, ``state-2-charges``, ``state-3-charges`` etc. (up to `lambda-dynamics-group-type1-n-states`) must be defined. They correspond to each protonation state of the titratable residue. **Note: The number of charge values must be the same as the number of atoms in the corresponding** ``atom-set``. **Note: The number of charge values must be the same as the number of atoms in the corresponding** ``atom-set``. 

- **lambda-dynamics-group-type1-state-1-reference-pka**

   - (0) Reference $`\text{p}K_{\text{a}}`$ of the model compound. If `lambda-dynamics-group-type1-n-states = 1`, this is simply a reference $`\text{p}K_{\text{a}}`$ value for the group. In the case of multisite groups, one of the states has to have reference $`\text{p}K_{\text{a}}`$ equal to the simulation pH ``lambda-dynamics-simulation-ph``. More information of selecting the reference $`\text{p}K_{\text{a}}`$ for multisite group can be found in the constant pH paper. [1]

- **lambda-dynamics-group-type1-state-1-dvdl-coefficients**

   - Coefficients for a n-th order polynomial fit to $`dV/d\lambda`$ data, to describe the derivative of $`V^{\text{MM}}`$. Coefficients $`p_i`$ are provided from the highest to lowest order: 
   ```math
   dV/d\lambda(\lambda_1,\lambda_2,...,\lambda_{k-1}) = p_1 \lambda_1^n + p_2 \lambda_1^{n-1} \lambda_2 + p_3 \lambda_1^{n-2}     \lambda_2^2 + ... + p_l \lambda_{k-1}^n + p_{l+1} \lambda_1^{n-1} + ... + p_m \lambda_{k-1}^{n-1} + ... + p_t
   ```
   - More information on parametrization can be found in the constant pH paper [1]. If the ``group-type`` defines a buffer, then the ``dvdl-coefficients`` specify the coefficients for one buffer molecule. These coefficients are multiplied by ``buffer-residue-multiplier`` value to describe all buffers in the ``atom-set``.

The following entries consider an ``atom-set``, that defines a titratable group in the simulation system. Each ``atom-set`` is connected to a ``group-type`` by the name. Depending on the number of states of the ``group-type``, one or more $`\lambda`$-coordinates are assinged to each ``atom-set``.

- **lambda-dynamics-atom-set1-name**

   - Name for the atom set. The name must correspond to one of the mdp:`lambda-dynamics-group-type-name` entries. The corresponding ``group type`` values will be used to describe this atom set entry.

- **lambda-dynamics-atom-set1-index-group-name**

   - Name of group in the $`\tt{index.ndx}`$ file that defines the atoms of the titratable group. The index groups should include those atoms of the group that change charge upon deprotonation. There must be as many atoms in the index group as there are charges in the ``group-type-state-0-charges``, ``group-type-state-1-charges``, etc. entries of the corresponding ``group-type``.

- **lambda-dynamics-atom-set1-barrier**

   - (7.5) [kJ mol$`^{-1}`$] Height of the double well biasing potential for all $`\lambda`$-coordinates of this atom set [3].

- **lambda-dynamics-atom-set1-initial-lambda**

   - Initial $`\lambda`$-values for the atom set. There must be as many values supplied as there are protonation states defined in mdp:`lambda-dynamics-group-type1-n-states`.

- **lambda-dynamics-atom-set1-charge-restraint-group-index**

   - (-1) All groups part of the charge constraint should have same ``charge-restraint-group-index`` value.  This is only needed for selected groups if mdp:`lambda-dynamics-charge-constraints` = yes. 

- **lambda-dynamics-atom-set1-buffer-residue**

   - **no**: This is the default.
   
   - **yes**: If set to yes, this ``atom-set`` corresponds to a buffer group. Then, ``buffer-residue-multiplier`` needs to be set as well. An ``atom-set`` defined as a buffer residue will by default have reference pKa set to pH and the biasing potential set to zero.  

- **lambda-dynamics-atom-set1-buffer-residue-multiplier**

   - (1) Equals to number of buffer molecules in this buffer ``atom-set`` group. Multiplier will multiply the ``dvdl-coefficients`` of the corresponding ``group-type`` of the buffer. 


`gmx cphmd` command
---   
SYNOPSIS
```
gmx cphmd [-s [<.tpr>]] [-e [<.edr>]] [-o [<.xvg>]] [-begin <time>]
          [-end <time>] [-numplot <int>] [-[no]coordinate] [-[no]dvdl]
          [-[no]velocity] [-tu <enum>]

DESCRIPTION

gmx cphmd analyses and plots results from simulations run using the constant
pH implementation. Users can plot the lambda coordinate values, lambda
velocities and lambda dvdl from energy (.edr) files. A valid run input (.tpr)
with constant pH enabled is required. More analysis features might be added in
the future.

OPTIONS

Options to specify input files:

 -s      [<.tpr>]           (topol.tpr)
           Run input file with cpHMD enabled
 -e      [<.edr>]           (energy.edr)
           Energy file from cpHMD simulation

Options to specify output files:

 -o      [<.xvg>]           (cphmd.xvg)
           Base name for output files generated

Other options:

 -begin  <time>             (4.85108e-33)
           First frame (ps) to read from trajectory
 -end    <time>             (5.26165e+170)
           Last frame (ps) to read from trajectory
 -numplot <int>             (4)
           How many lambda coordinates should be combined into one plot
 -[no]coordinate            (yes)
           Whether coordinates should be plotted or not
 -[no]dvdl                  (no)
           Whether dvdl values should be plotted or not
 -[no]velocity              (no)
           Whether velocites should be plotted or not
 -tu     <enum>             (ps)
           Unit for time values: fs, ps, ns, us, ms, s
```
