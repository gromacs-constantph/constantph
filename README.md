# GROMACS constant pH code

This is preliminary version of the GROMACS constant pH code described in the [preprint](https://doi.org/10.26434/chemrxiv-2022-n025t-v2). The manual for the current version can be found [here](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md). The tutorials and example files can be found [here](https://gitlab.com/gromacs-constantph/tutorials). The modified force fields needed for constnat pH, as explained in the [preprint](https://doi.org/10.26434/chemrxiv-2022-c6lg2), can be found [here](https://gitlab.com/gromacs-constantph/force-fields).

Please, carefully read the [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) before using the code. If you want to report a bug or suggest a feature,
please refer to the [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) for details.

**Please note, that the code has not been properly tested yet and some unexpected behaviour might be observed. The code is not of production quality yet, only after the publication can we guarantee the quality of our code for production runs. Therefore, we would kindly ask you to avoid publishing results obtained with the current version of the code, but confirm your results with the final version instead when it is out. Please also note, that we are currently working on merging the code into GROMACS. We therefore kindly ask you to not base your development on the current version and wait for the GROMACS version.**

**KNOWN ISSUES**
- Currently, separate PME ranks are not supported with MPI
- Code is not sufficiently covered with tests and some standard GROMACS test might be broken. Thus, `make check` might end up with errors. You can just ignore them
- `gmx mdrun` does not support `-update gpu` option

If using `GROMACS constant pH MD` in your research, please cite:
```
	[1] Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., Journal of Chemical Theory and Computation 18.10 (2022): 6148-6160.
	[2] Best practices in constant pH MD simulations: accuracy and sampling, Buslaev P., Aho N., Jansen A., Bauer P., Groenhof G., Hess B., Journal of Chemical Theory and Computation, 18(10), 6134-6147.
```

The code has been developed by:

1. Noora Aho (noora.s.aho at jyu.fi)
2. Buslaev Pavel (pavel.i.buslaev at jyu.fi)
3. Anton Jansen
4. Paul Bauer (pbauer at kth.se)
5. Gerrit Groenhof (gerrit.x.groenhof at jyu.fi)
6. Berk Hess (hess at kth.se)
