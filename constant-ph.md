WARNING BEFORE USAGE
---

**Please note, that the code has not been properly tested yet and some unexpected behaviour might be observed. We are currently cleaning up the code, and some small changes in the input organisation might happen. The code is not of production quality yet, only after the publication can we guarantee the quality of our code for production runs. Therefore, we would kindly ask you to avoid publishing results obtained with the current version of the code, but confirm your results with the final version instead when it is out.**

**KNOWN ISSUES**
- Currently, separate PME ranks are not supported with MPI
- Code is not sufficiently covered with tests and some standard GROMACS test might be broken. Thus, `make check` might end up with errors. You can just ignore them.
- `cphmd` command currently does not work with `.edr` files saved as separate part after continuation (when `-noappend` is used in `mdrun`). We currently recommend all the users to append data to the initial files when continuing simulations. 

Installation
---

The GROMACS constant pH installation doesn't differ from normal GROMACS installtion. So follow [standard GROMACS routines](https://manual.gromacs.org/documentation/current/install-guide/index.html).

Bug reports
---
If you observed an unexpected code behaviour, please open an issue on the Gitlab pages and provide the complete description of the bug observed.

Feature suggestion
---
If you want to suggest a feature, please contact one of the developers by email.

Constant pH Molecular Dynamics
------------------------------

Constant pH molecular dynamics (CpHMD) enables MD simulations at a fixed pH. During the CpHMD simulation, the protonation states of selected titratable groups are allowed to change, depending on the pH and electrostatic interactions between the group and the environment. This means, that during a CpHMD simulation, the titratable groups can continuously change between protonated and deprotonated states. At the force field level such transformations require the change of partial charges, Lennard-Jones, and bonded parameters of atoms that constitute the titratable groups. Current implementation takes into account only the electrostatic interactions due to the small contributions of other interaction types to free energy difference between protonated and deprotonated states [1]. Thus, only the partial charges are changed. This is achieved by 
so called $`\lambda`$-dynamics [2]. 
More details can be found in the original paper [1].

Basics of the method
---

Basics of the method can be found [here](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/manual/constantph_general.md)

Multisite titratable groups and charge constraints
---

Details on how titratable groups with multiple protonation sites (e.g. Histidine) and charge fluctuations are treated can be found [here](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/manual/constantph_features.md)

Usage
---

Details on the usage of the method (simulation system, input, output) can be found [here](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/manual/constantph_usage.md)

References
---
[1]: https://doi.org/10.26434/chemrxiv-2022-n025t-v2  Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., ChemRxiv, 2022

[2]: https://doi.org/10.1063/1.472109 $`\lambda`$‐dynamics: A new approach to free energy calculations, Kong X and Brooks III C.L., JCP, 1996

